# Catamod Editor

Editor for Unnamed CataBN mod.

# How to build and run

First of all, clone [main repo](https://gitlab.com/somobu/catamod) and
maybe [content repo](https://gitlab.com/somobu/catamod-data) too.

Clone this repo to a `./editor` directory in the main repo:

```bash
git clone https://gitlab.com/somobu/catamod-editor.git editor
```

You'll need get some web-dev tools like `node` and `npm`.

Then you can install dependencies:

```bash
npm install
```

And run the editor:

```bash
npm start
```

## Important usage notes

- you have to manually generate schema.json on the first run: to do so, run editor, select all mods (including obsolete
  ones), load it, and press [G] button in top right corner. Editor will hang up for a minute or two -- its normal;

# License

[GNU AGPLv3](https://gitlab.com/somobu/catamod-editor/-/blob/master/LICENSE)
