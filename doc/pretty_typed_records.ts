import {field_description} from "../src/main/schema";

// Typical types

const ref_category_t: field_description = {t: "ref", tbl: "ITEM_CATEGORY"}
const ref_flag_t: field_description = {t: "ref", tbl: "json_flag"}
const ref_generic_t: field_description = {t: "ref", tbl: "GENERIC"}
const ref_item_action_t: field_description = {t: "ref", tbl: "item_action"}

const color_t: field_description = {
    t: "enum",
    enm: [
        "blue", "brown", "cyan", "green", "magenta", "pink", "red", "white", "yellow",
        "dark_gray",
        "light_cyan", "light_gray", "light_green", "light_red",
        // TODO: more colors to fill
    ]
}

const damage_t: field_description = {
    t: "dict",
    dct: {
        "damage_type": todo_t, // TODO: clarify
        // TODO: more fields to fill
    }
}

const flags_array_t: field_description = {
    t: "array",
    arr: ref_flag_t
}

const material_array_t: field_description = {
    t: "array",
    arr: {
        t: "id",
        tbl: "material",
        fld: "id"
    }
}

const translation_dict_t: field_description = {
    t: "dict",
    dct: {
        "str": str_t,
        "str_pl": str_t
    }
}

const type_t: field_description = {
    t: "enum",
    enm: ["AMMO", "BIONIC_ITEM"]
}



export const typed_records: { [key: string]: { [key: string]: field_description | undefined } } = {

    "AMMO": {
        "abstract": {t: "ref", tbl: "AMMO"},
        "ammo_type": {t: "ref", tbl: "ammunition_type"},
        "bashing": int_t,
        "casing": todo_t, // TODO: is it ref?
        "category": ref_category_t,
        "color": color_t,
        "container": {t: "ref", tbl: "CONTAINER"},
        "copy-from": {t: "ref", tbl: "AMMO"},
        "count": int_t,
        "cutting": int_t,
        "damage": damage_t,
        "delete": {
            t: "dict",
            dct: {
                "effects": {t: "array", arr: {t: "ref", tbl: "ammo_effect"}},
                "flags": {t: "array", arr: ref_flag_t},
                "casing": ref_generic_t,
            }
        },
        "description": str_t,
        "dispersion": int_t,
        "dont_recover_one_in": int_t,
        "drop": ref_generic_t,
        "drop_action": todo_t, // TODO: weird struct, type it plz,
        "effects": {t: "array", arr: {t: "ref", tbl: "ammo_effect"}},
        "explode_in_fire": bool_t,
        "explosion": {t: "dict", dct: {"power": int_t, "shrapnel": int_t,}},
        "extend": {t: "dict", dct: {"effects": {t: "array", arr: {t: "ref", tbl: "ammo_effect"}}}},
        "flags": flags_array_t,
        "id": id_t,
        "fuel": todo_t,
        "looks_like": {t: "ref", tbl: "AMMO"},
        "loudness": int_t,
        "material": material_array_t,
        "name": translation_dict_t,
        "phase": {t: "enum", enm: ["solid", "liquid", "gas"]},
        "price": int_t,
        "price_postapoc": int_t,
        "proportional": todo_t,
        "qualities": todo_t,
        "range": int_t,
        "recoil": int_t,
        "relative": todo_t,
        "seed_data": todo_t,
        "shape": todo_t,
        "show_stats": bool_t,
        "stack_size": int_t,
        "symbol": chr_t,
        "to_hit": int_t,
        "type": type_t,
        "use_action": ref_item_action_t,
        "volume": str_t,
        "weight": str_t,
    },

    "BIONIC_ITEM": {
        "abstract": {t: "ref", tbl: "BIONIC_ITEM"},
        "bionic_id": todo_t,
        "bashing": int_t,
        "category": ref_category_t,
        "color": color_t,
        "copy-from": {t: "ref", tbl: "BIONIC_ITEM"},
        "description": str_t,
        "difficulty": int_t,
        "extend": todo_t,
        "faults": {t: "array", arr: {t: "enum", enm: ["fault_bionic_nonsterile"]}},
        "flags": flags_array_t,
        "id": id_t,
        "is_upgrade": bool_t,
        "installation_data": todo_t,
        "looks_like": {t: "ref", tbl: "BIONIC_ITEM"},
        "material": material_array_t,
        "name": translation_dict_t,
        "price": int_t,
        "price_postapoc": int_t,
        "symbol": chr_t,
        "type": type_t,
        "use_action": ref_item_action_t,
        "volume": str_t,
        "weight": str_t,
    }

}
