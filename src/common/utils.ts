import {OwnRecordId, Record} from "./types";

export function record_id(record: Record): OwnRecordId {
    return record._mod + ":" + record.type + ":" + record.id
}

export function raw_id(id: OwnRecordId): string {
    return id.substring(id.lastIndexOf(":") + 1)
}
