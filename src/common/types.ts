export type OwnRecordId = string

export type Record = {
    type: string;

    /**
     * Record ID. Expected to be unique across records of the same type
     */
    id: string;

    /**
     * Our own id. Should be unique across all records
     */
    _id?: OwnRecordId;
    _mod?: string;
    _file?: string;
    _modified?: boolean;
};

export type Record_ModInfo = Record & {
    name: string;

    description?: string;
    category?: string;
    obsolete?: boolean;
    core?: boolean;

    path?: string;
    dependencies?: string[];

    _modinfo_dir?: string;
};

export type UserDeps = {
    records: { [key: string]: Record[] },
    dep_tree: { [key: string]: string[] }
}
