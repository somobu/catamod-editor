import {css, styled} from 'styled-components';
import {useEffect, useState} from "react";

import './App.css';
import Greeter from "./Greeter";
import {apicall} from "./util";
import {ModLoadingResult} from "../main/reader";
import Editor from "./Editor";
import {Record_ModInfo} from "../common/types";

const LoadingRoot = styled.div`
    height: calc(100vh);

    display: flex;
    align-items: center;
    align-content: center;
    justify-content: center;
`

export default function App() {
    const [loading, setLoading] = useState(true);
    const [modlist, setModlist] = useState([] as Record_ModInfo[]);
    const [modload, setModload] = useState(null as ModLoadingResult);

    useEffect(() => {
        apicall("reset-records", null).then(() => {
            apicall("get-modlist", null).then((e: Record_ModInfo[]) => {
                setModlist(e)
                setLoading(false)
            });
        })
    }, [])

    if (loading) {
        return <LoadingRoot>Loading...</LoadingRoot>
    } else if (modload === null) {
        return <Greeter
            modlist={modlist}
            onSelectionConfirmed={(selection: String[]) => {
                setLoading(true)

                const mods_to_load = selection
                    .map((mod_id) => {
                        return modlist.filter((mod) => mod.id === mod_id)[0]
                    })

                apicall('load-mods', mods_to_load).then((e: ModLoadingResult) => {
                    console.log("Modloaded:", e)
                    setLoading(false)
                    setModload(e)
                });
            }}
        />;
    } else {
        return <Editor summary={modload}/>;
    }
}
