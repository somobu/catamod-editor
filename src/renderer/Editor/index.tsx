import EntriesPanel from "./EntriesPanel";
import DeptreePopup from "./DeptreePopup";
import {css, styled} from "styled-components";
import {useState} from "react";
import {apicall} from "../util";
import {OwnRecordId} from "../../common/types";


const Root = styled.div`
    height: calc(100vh - 16px);

    display: flex;
    flex-direction: column;
    gap: 8px;

    padding: 8px;
    z-index: 3;
`;

const TabsRow = styled.div`
    height: 32px;

    display: flex;
    flex-direction: row;
    gap: 8px;
`

const Spacer = styled.div`
    flex-grow: 1;
`

const FixedSpacer = styled.div`
    width: 8px;
`

const Tab = styled.div<{ $focused?: boolean }>`
    border: 1px solid black;
    height: 28px;

    padding-left: 16px;
    padding-right: 16px;

    display: flex;
    align-items: center;
    align-content: center;
    justify-content: center;

    font-size: 18px;

    cursor: pointer;

    &:hover {
        background-color: #EEE;
    }

    ${props => props.$focused && css`
        background: #BF4F74;
        color: white;

        &:hover {
            background-color: #db5c86;
        }
    `}
`


const Editor = ({summary}) => {
    const [dep, setDep] = useState(null as (OwnRecordId[] | null));

    const saveData = () => {
        apicall('save-records', null).then((e) => console.log("Data saved!"))
    }

    const genSchema = () => {
        apicall('gen-schema', null).then((e) => console.log("Schemagen done!"))
    }
    
    const writeDeps = () => {
        apicall('write-deps', null).then((e) => console.log("Deps written!"))
    }
    
    return <>
        <Root>
            <TabsRow>
                <Tab $focused={true}>Raw entries</Tab>
                <Tab>Dummy tab</Tab>
                <Tab className={"disabled"}>Translations</Tab>
                <Spacer/>
                <Tab className={"disabled"}>Undo</Tab>
                <Tab className={"disabled"}>Redo</Tab>
                <FixedSpacer/>
                <Tab onClick={saveData}>Save</Tab>
                <Tab className={"disabled"}>Reset</Tab>
                <Tab className={"disabled"}>Exit</Tab>
                <FixedSpacer/>
                <Tab onClick={genSchema}>G</Tab>
                <Tab onClick={writeDeps}>D</Tab>
            </TabsRow>

            <EntriesPanel
                summary={summary}
                handleDeptreePopup={(ids) => setDep(ids)}/>
        </Root>
        {dep != null ? <DeptreePopup ids={dep} hidePopup={() => setDep(null)}></DeptreePopup> : <></>}
    </>
}


export default Editor; 
