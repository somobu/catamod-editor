import styled from "styled-components";
import {useState} from "react";

const Root = styled.div`
  cursor: pointer;

  &:hover {
    background-color: #fff4f8;
  }
`

const Badge = styled.div`
  height: 32px;
  width: 8px;
`

const Title = styled.div`
  height: 32px;

  display: flex;
  align-items: center;

  margin-left: 8px;
  margin-right: 8px;

  font-size: 20px;
`

const Input = styled.input`
  width: 20px;
  height: 20px;
`

const Line = styled.div`
  height: 1px;

  margin-left: 4px;
  margin-right: 4px;

  background-color: #DDD;
`

const Spacer = styled.div`
  flex-grow: 1;
`

const Content = styled.pre`
  padding: 4px;
  border: 1px solid lightgray;
  overflow: scroll;
  max-height: 320px;
`

const Row = ({lindep, children, selected, toggleSelection}) => {
    const [content, showContent] = useState(false);

    const lineWidth = 16 * lindep.ident;
    const contentPad = 48 + lineWidth;

    return <Root onClick={() => showContent(!content)}>
        <Title>
            <Badge style={{backgroundColor: lindep.badge}}/>
            <Input
                type={"checkbox"}
                disabled={!lindep.checkable}
                checked={selected}
                onClick={(e) => e.stopPropagation()}
                onChange={() => toggleSelection(lindep.own_id)}/>
            <Line style={{width: lineWidth + "px"}}/>
            {lindep.checkable
                ? <>{lindep.title}<Spacer/>{lindep.mod}</>
                : <>
                    <span style={{color: "gray"}}>{lindep.title}</span>
                    <Spacer/>
                    <span style={{color: "gray"}}>{lindep.mod}</span>
                </>
            }
        </Title>
        {content && children && lindep.checkable
            ? <Content onClick={(e) => e.stopPropagation()} style={{marginLeft: contentPad + "px"}}>{children}</Content>
            : <></>
        }
    </Root>;
}

export default Row;
