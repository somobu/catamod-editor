import styled from "styled-components";
import Row from "./Row";
import {useEffect, useState} from "react";
import {apicall} from "../../util";
import Scrollbars from "react-custom-scrollbars-2";
import {raw_id} from "../../../common/utils";
import {OwnRecordId, UserDeps} from "../../../common/types";

const Root = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  background-color: #000000CC;

  cursor: pointer;
  z-index: 10;
`;

const Panel = styled.div`
  position: absolute;
  top: max(12px, calc(50% - 480px));
  bottom: max(12px, calc(50% - 480px));
  left: calc(50% - 480px);
  right: calc(50% - 480px);

  display: flex;
  flex-direction: column;

  padding: 0;

  border-radius: 8px;

  background-color: #FFF;
  cursor: auto;
`

const TitleContainer = styled.div`
  height: 32px;

  display: flex;
  flex-direction: row;

  margin: 16px 16px 0;
`

const Title = styled.span`
  font-weight: 600;
  font-size: 150%;
`

const Spacer = styled.div`
  flex-grow: 1;
`

const HeaderButton = styled.div`
  height: 32px;

  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;

  padding-left: 12px;
  padding-right: 12px;

  cursor: pointer;

  color: white;
  background-color: #555;
  border-radius: 4px;

  &:hover {
    background-color: #444;
  }
`

const SubtitleContainer = styled.div`
  height: 24px;

  display: flex;
  flex-direction: row;
  align-items: baseline;

  padding: 16px 16px 12px;

  border-bottom: 1px solid gray;
`

const SubtitleSpacer = styled.div`
  width: 8px;
`


type LinearDep = {
    id: number;
    own_id: string
    ident: number;
    title: string;
    badge: string | null;
    message: string;
    checkable: boolean;
    mod: string;
}


function unwrap_name(value: any): string {
    if (typeof value == "string") return value
    if (value["str"] != null) return value["str"]
    if (value["str_sp"] != null) return value["str_sp"]
    return JSON.stringify(value)
}

function findAllDepsOf(d: UserDeps, id: string): string[] {

    let found_ids = []
    let current_step_ids = [raw_id(id)]

    while (current_step_ids.length > 0) {
        let next_step_ids = []

        current_step_ids.forEach(i => {
            found_ids.push(i);

            (d.dep_tree[i] || []).forEach((e) => {
                if (!found_ids.includes(e) && !next_step_ids.includes(e)) {
                    next_step_ids.push(e)
                }
            })
        })

        current_step_ids = next_step_ids
    }

    return found_ids.filter(i => i !== raw_id(id))
}

const DeptreePopup = ({ids, hidePopup}) => {

    const [data, setData] = useState({} as UserDeps)
    const [tree, setTree] = useState([] as LinearDep[])
    const [selection, setSelection] = useState([] as OwnRecordId[])
    const [hideDupes, setHideDupes] = useState(false)
    const [hideItemGroups, setHideItemGroups] = useState(true)

    const anySelected = selection.length > 0

    const toggleSelection = (id) => {
        let deps = findAllDepsOf(data, id)

        if (selection.includes(id)) { // Deselect
            let newSelection = selection.filter((v) => !deps.includes(raw_id(v)))
            newSelection = newSelection.filter(v => v != id)
            setSelection(newSelection)
        } else { // Select
            let newSelection = [...selection, id]

            Object.entries(data.records).forEach(([k, v], _) => {
                if (deps.includes(k)) newSelection = [...newSelection, ...v.map(e => e._id!!)]
            })

            deps.forEach((i) => {
                if (!newSelection.includes(i)) newSelection.push(i)
            })

            setSelection(newSelection)
        }
    }

    useEffect(() => {
        apicall('get-deps', ids).then((rz: UserDeps) => {
            setData(rz)

            let shown_ids = []

            let next_ids = (ids as string[]).map(i => raw_id(i))
            let next_ids_parents = [] as LinearDep[]

            let tree = [] as LinearDep[]
            let current_depth = 0

            let glob_idx = 0

            while (next_ids.length != 0) {
                let new_next_ids = [] as string[]
                let new_next_ids_parents = [] as LinearDep[]

                next_ids.forEach((id, idx) => {
                    let records = rz.records[id]

                    let shown = shown_ids.includes(id)
                    let non_unique = records.length > 1
                    let color = "#" + (Math.floor(Math.random() * 2 ** 16)).toString(16).padStart(6, '0');

                    let pointer_entry = null
                    let is_item_group = false

                    records.forEach((record) => {

                        if (record.type == "item_group") is_item_group = true

                        // Construct record
                        let entry: LinearDep = {
                            id: glob_idx,
                            own_id: record._id!!,
                            ident: current_depth,
                            title: "NO RECORDS (is this a BUG?)",
                            badge: non_unique ? color : null,
                            message: shown ? "" : JSON.stringify(record, null, "  "),
                            checkable: !shown,
                            mod: record._mod!!
                        };

                        if (record["name"] != null) {
                            entry.title = record.type + ": \"" + unwrap_name(record["name"]) + "\""
                        } else {
                            entry.title = record.type + ": " + record.id
                        }

                        // Insert record at proper position in tree
                        let insertion_index = 1
                        if (idx < next_ids_parents.length) insertion_index = tree.indexOf(next_ids_parents[idx])
                        tree.splice(insertion_index + 1, 0, entry);

                        if (pointer_entry == null) pointer_entry = entry;

                        glob_idx++;
                    });

                    if (!shown) {
                        shown_ids.push(id);

                        // Set pointers
                        if (!hideItemGroups || !is_item_group) {
                            (rz.dep_tree[id] || []).forEach(id => {
                                new_next_ids.push(id);
                                new_next_ids_parents.push(pointer_entry);
                            });
                        }
                    }
                })

                next_ids = new_next_ids;
                next_ids_parents = new_next_ids_parents;
                current_depth += 1;
            }

            setTree(tree)
        })
    }, [hideItemGroups])

    return <Root onClick={hidePopup}>
        <Panel onClick={(e) => {
            e.stopPropagation()
        }}>
            <TitleContainer>
                {anySelected
                    ? <><HeaderButton>Move to mod</HeaderButton></>
                    : <><Title>Reverse dependency tree</Title></>
                }
            </TitleContainer>

            <SubtitleContainer>
                {anySelected
                    ? <></>
                    : <>
                        <input
                            type="checkbox"
                            id="obsolete"
                            name="obsolete"
                            checked={hideDupes}
                            onChange={(event) => setHideDupes(event.target.checked)}
                        />
                        <label htmlFor="obsolete">Hide dupes</label>

                        <SubtitleSpacer/>

                        <input
                            type="checkbox"
                            id="hide_item_groups"
                            name="hide_item_groups"
                            checked={hideItemGroups}
                            onChange={(event) => setHideItemGroups(event.target.checked)}
                        />
                        <label htmlFor="hide_item_groups">Hide item groups' children</label>
                    </>
                }
            </SubtitleContainer>

            <Scrollbars>
                {tree.map((v) => {
                    if (v.checkable || !hideDupes) {
                        return <Row
                            key={v.id}
                            lindep={v}
                            selected={selection.includes(v.own_id)}
                            toggleSelection={toggleSelection}>
                            {v.message}
                        </Row>
                    }
                })}
            </Scrollbars>
        </Panel>
    </Root>;
}

export default DeptreePopup;
