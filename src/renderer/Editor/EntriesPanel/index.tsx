import styled, {css} from "styled-components";
import Scrollbars from "react-custom-scrollbars-2";
import {useEffect, useState} from "react";
import {apicall} from "../../util";
import {ModLoadingResult} from "../../../main/reader";
import {internalRecordFields} from "../../../common/consts";
import {Record} from "../../../common/types";

const Root = styled.div`
  border: 1px solid #777;
  border-radius: 2px;
  flex-grow: 1;

  display: flex;
  flex-direction: column;

  color: #555;
`;

const Header = styled.div`
  height: 48px;

  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 12px;

  padding-left: 8px;
  padding-right: 8px;
`

const HeaderSelect = styled.select`
  height: 32px;

  padding-left: 12px;
  padding-right: 12px;

  cursor: pointer;

  color: white;
  background-color: #555;
  border-radius: 4px;
`

const HeaderButton = styled.div`
  height: 32px;

  display: flex;
  align-items: center;
  align-content: center;
  justify-content: center;

  padding-left: 12px;
  padding-right: 12px;

  cursor: pointer;

  color: white;
  background-color: #555;
  border-radius: 4px;

  &:hover {
    background-color: #444;
  }
`

const HeaderInput = styled.input`
  height: 32px;

  color: white;
  background-color: #555;
  border-radius: 4px;

  padding-left: 12px;
  padding-right: 12px;
`

const Scrollbar = styled(Scrollbars)`
  border-bottom: 1px solid black;
`

const Table = styled.table`
  margin: 0;
  padding: 0;
  border-spacing: 0;
  border-bottom: 1px solid black;
`

const Row = styled.tr`
  & > td:nth-child(1), & > td:nth-child(2) {
    position: sticky;
    z-index: 1;
    font-weight: 600;
  }

  & > td:nth-child(1) {
    left: 0;
  }

  & > td:nth-child(2) {
    left: 33px;
  }
`

const HeadRow = styled(Row)`
  height: 24px;
  border: none;

  position: sticky;
  top: 0;
  z-index: 2;

  & > td {
    background: #EEE;
    border-top: 1px solid black;
    border-bottom: 1px solid black;
    font-weight: 600;
  }
`

const DataRow = styled(Row)<{ $modified?: boolean }>`
  height: 24px;
  border: none;

  ${props => props.$modified && css`
    font-weight: 600;
  `}
  &:hover {
    background-color: lightyellow;
  }

  &:nth-child(even) > td {
    background-color: #FFF;
  }

  &:nth-child(odd) > td {
    background-color: #F5F5F5;
  }
`

const Td = styled.td`
  max-width: 256px;
  height: 28px;

  padding: 2px 4px 2px 8px;
  border: 1px white;

  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;

  border-right: 1px solid black;
`;

const LnkBtn = styled.button`
  border: 1px solid black;
  background-color: #BBB;

  cursor: pointer;

  &:hover {
    background-color: #DDD;
  }
`;

const BottomRow = styled.div`
  padding: 8px;
`


/**
 * Fields should be hidden for all record types
 */
const global_ignored = [
    "type", // Record type is shown in type selector
    "id",   // We'll show it manually (1st column)

    // Ignore all kinds of comments
    "//", "//~", "//1", "//2", "//3", "//4", "//5",

    // Our internal stuff
    ...internalRecordFields
]

const prettify = (o: any): String => {
    if (typeof o == "string") return o
    if (Array.isArray(o) && o.length == 1) return prettify(o[0])

    return JSON.stringify(o, null, 2)
}

const default_initial_rectype = "MOD_INFO"

const EntriesPanel = ({handleDeptreePopup, summary}) => {
    const [loading, setLoading] = useState(true)

    const [type, setType] = useState(default_initial_rectype)
    const [records, setRecords] = useState({
        titles: [] as String[],
        rows: [] as Record[]
    })
    const [filter, setFilter] = useState("")

    const [selection, setSelection] = useState([] as string[])

    const loadRecords = () => {
        setLoading(true)
        apicall('get-records', type).then((e: Record[]) => {
            let fields: String[] = []

            e.forEach((record) => {
                Object.entries(record)
                    .forEach(([key, val]) => {
                        if (!fields.includes(key)) fields.push(key)
                    })
            });

            fields = fields.filter((val) => !global_ignored.includes(val))
            fields.sort((a, b) => a.localeCompare(b))

            e.sort((a, b) => (a.id + "").localeCompare(b.id + ""))

            setRecords({titles: fields, rows: e})
            setLoading(false)
        })
    }

    useEffect(loadRecords, [type])

    const toggleSelection = (id: string) => {
        if (selection.includes(id)) setSelection(selection.filter((val) => val !== id))
        else setSelection([...selection, id])
    }

    const isSomeSelected = selection.length > 0
    const isAllSelected = records.rows.every((row) => selection.includes(row.id));

    const toggleAll = () => {
        if (isAllSelected) setSelection([])
        else setSelection(records.rows.map((row) => row.id))
    }

    const deleteSelected = () => {
        const toDelete = selection.map((value) => records.rows.find((row) => row.id == value)?._id)
        apicall('delete-records', toDelete).then(() => {
            console.log("Records deleted (UI)")
            loadRecords()
            setSelection([])
        })
    }

    return <Root>
        <Header>
            {
                selection.length == 0
                    ? <>
                        <HeaderButton className={"disabled"}>Mods: all</HeaderButton>
                        <HeaderSelect
                            disabled={loading}
                            onChange={(event) => setType(event.target.value)}
                            value={type}
                        >
                            {(summary as ModLoadingResult).known_types.map((type) => {
                                return <option key={type} value={type}>{type}</option>
                            })}
                        </HeaderSelect>
                        <HeaderInput
                            placeholder={"Filter"}
                            value={filter}
                            onChange={(e) => setFilter(e.target.value)}
                        />
                    </>
                    : <>
                        Selection:
                        <HeaderButton className={"disabled"}>Move</HeaderButton>
                        <HeaderButton onClick={deleteSelected}>Delete</HeaderButton>
                    </>
            }


        </Header>

        <Scrollbar>
            <Table>
                <thead>
                <HeadRow>
                    <Td>
                        <input
                            type="checkbox"
                            checked={isAllSelected}
                            onChange={() => toggleAll()}
                        />
                    </Td>
                    <Td>id</Td>

                    {isSomeSelected ? <></> : <Td>act</Td>}

                    {records.titles.map((title) => <Td key={title}>{title}</Td>)}
                </HeadRow>
                </thead>

                <tbody>

                {records.rows
                    .filter((row) => Object.entries(row).some(([, val]) => {
                        return String(val).includes(filter)
                    }))
                    .map((row, idx) => <DataRow key={idx} $modified={row._modified || false}>
                            <Td>
                                <input
                                    type="checkbox"
                                    checked={selection.includes(row.id)}
                                    onChange={() => toggleSelection(row.id)}
                                />
                            </Td>
                            <Td>{row.id}</Td>

                            {isSomeSelected
                                ? <></>
                                : <Td><LnkBtn onClick={() => handleDeptreePopup([row._id])}>#</LnkBtn></Td>
                            }

                            {records.titles.map((key) =>
                                <Td key={idx + ":" + key} title={JSON.stringify(row[key], null, 4)}>
                                    {prettify(row[key])}
                                </Td>
                            )}
                        </DataRow>
                    )}

                </tbody>
            </Table>
        </Scrollbar>

        <BottomRow>
            {records.rows.length} entries total {isSomeSelected ? " / " + selection.length + " selected" : ""}
        </BottomRow>
    </Root>;
}

export default EntriesPanel;
