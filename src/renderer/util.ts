export const apicall = (method, args: unknown) => {
    return new Promise((resolve, reject) => {
        window.electron.ipcRenderer.once(method, (arg) => resolve(arg));
        window.electron.ipcRenderer.sendMessage(method, args);
    });
}

