import styled, {css} from "styled-components";
import Scrollbars from "react-custom-scrollbars-2";
import {useState} from "react";
import {Record_ModInfo} from "../../common/types";

const Root = styled.div`
    height: calc(100vh);
    margin: 0;
    padding: 0;

    display: flex;
    align-items: center;
    justify-content: center;
`

const Container = styled.div`
    width: 480px;
    height: 720px;

    display: flex;
    flex-direction: column;

    border: 1px solid #555;
    border-radius: 4px;
    padding: 16px;
`

const FiltersBox = styled.div`
    height: 32px;

    display: flex;
    flex-direction: row;
    gap: 4px;
`

const FilterInput = styled.input`

`

const FilterSpacer = styled.div`
    flex-grow: 1;
`

const FilterDiv = styled.div`
    padding-left: 12px;
    padding-right: 12px;

    display: flex;
    align-items: baseline;
    justify-content: center;
`

const FilterButton = styled.button`
    padding-left: 12px;
    padding-right: 12px;
`

const Scrollbar = styled(Scrollbars)`
    flex-grow: 1;
`

const ModEntryRoot = styled.div`
    min-height: 40px;
    line-height: 40px;
    padding-left: 12px;

    box-sizing: border-box;

    border: 1px solid transparent;
    border-radius: 4px;

    cursor: pointer;

    &:hover {
        border: 1px solid black;
    }

    ${props => props.$activate && css`
        background: bisque;
    `}
`

const ModEntry = ({active, children, onClick}) => {
    return <ModEntryRoot $activate={active} onClick={onClick}>
        {children}
    </ModEntryRoot>
}

const Button = styled.button`
    min-height: 40px;
    margin-top: 8px;

    cursor: pointer;
`

function deepSelect(modlist: Record_ModInfo[], target: String): String[] {
    return [target, ...modlist
        .filter((value) => value.id === target)[0]
        .dependencies
        ?.flatMap((value) => [...deepSelect(modlist, value)])];
}

function getDependents(modlist: Record_ModInfo[], target: String): String[] {
    return modlist
        .filter((value) => value.dependencies?.includes(target))
        .flatMap((value) => [value.id, ...getDependents(modlist, value.id)]);
}


const Greeter = ({modlist, onSelectionConfirmed}) => {
    const [selection, setSelection] = useState(["core"] as String[]);
    const [filter, setFilter] = useState("");
    const [obsolete, setObsolete] = useState(false)

    const proper_modlist = obsolete
        ? (modlist as Record_ModInfo[])
        : (modlist as Record_ModInfo[]).filter((mod) => !(mod.obsolete || false))

    const isSelected = (id) => selection.includes(id);
    const toggleSelection = (id) => {
        if (isSelected(id)) {
            const excluded = [id, ...getDependents(proper_modlist, id)];
            setSelection(selection.filter((val) => !excluded.includes(val)))
        } else {
            const new_sel = deepSelect(proper_modlist, id);
            const filtered = selection.filter((val) => !new_sel.includes(val));
            setSelection([...new_sel, ...filtered])
        }
    };

    const toggleAll = () => {
        const allSelected = proper_modlist.every((mod) => selection.includes(mod.id));

        if (allSelected) setSelection([])
        else setSelection(proper_modlist.map((mod) => mod.id))
    };

    return <Root>
        <Container>
            <b style={{marginBottom: "8px"}}>Select mods you wanna work with</b>

            <FiltersBox>
                <FilterInput
                    placeholder={"Filter by name or id"}
                    value={filter}
                    onChange={(evt) => setFilter(evt.target.value)}/>
                <FilterButton onClick={() => setFilter("")}>x</FilterButton>
                <FilterSpacer/>
                <FilterDiv>
                    <input
                        type="checkbox"
                        id="obsolete"
                        name="obsolete"
                        checked={obsolete}
                        onChange={(event) => setObsolete(event.target.checked)}
                    />
                    <label htmlFor="obsolete">Obsolete</label>
                </FilterDiv>
                <FilterButton onClick={toggleAll}>All</FilterButton>
            </FiltersBox>

            <hr/>

            <Scrollbar>
                {
                    (proper_modlist as Record_ModInfo[])
                        .filter((mod) => {
                            return mod.id.includes(filter)
                                || mod.name.includes(filter)
                        })
                        .map((mod) => {
                            return <ModEntry
                                key={mod.id}
                                onClick={() => toggleSelection(mod.id)}
                                active={isSelected(mod.id)}>
                                {mod.name} [{mod.id}]
                            </ModEntry>
                        })
                }
            </Scrollbar>

            <Button onClick={() => onSelectionConfirmed(selection)}>Load</Button>
        </Container>
    </Root>
}

export default Greeter;
