import {field_description, typed_records} from "./schema";
import {on_record_read} from "./conversion";
import {open} from "fs/promises";

import {OwnRecordId, Record, UserDeps} from "../common/types";
import {raw_id, record_id} from "../common/utils";

const DEBUG_CONFLICTING_IDS = false;

export let mod_basedir: { [key: string]: string } = {}

let records_by_common_id: { [key: string]: Record[] } = {}
let records_by_unique_id: { [key: OwnRecordId]: Record } = {}
let records_by_file: { [key: string]: Record[] } = {}
let records_by_type: { [key: string]: Record[] } = {}

export let known_ids: string[] = []
let known_files: string[] = []
let known_types: string[] = []
let modified_files: string[] = []

export let dep_tree2: { [key: string]: string[] } = {} // maps base to dependent records


/**
 * Append `value` to an array in `dict` under given `key`
 */
function append_arrdict(dict, key, value) {
    if (dict[key] != null && dict[key].includes(value)) return;
    dict[key] = [...(dict[key] || []), value]
}

function drop_arrdict(dict, key, value) {
    dict[key] = dict[key].filter((v) => v !== value)
}

export function accumulate(mod_id: string, source: string, record: Record) {
    // File must be assigned as soon as possible
    record._file = source;
    record._mod = mod_id;

    // First of all, ensure record has identifier
    let records = on_record_read(record)

    records.forEach((record) => {

        // Assign our data
        record._id = record_id(record);

        // Register file and type
        if (!known_ids.includes(record.id)) known_ids.push(record.id)
        if (!known_files.includes(source)) known_files.push(source)
        if (!known_types.includes(record.type)) known_types.push(record.type)

        // Register record id
        if (DEBUG_CONFLICTING_IDS && records_by_unique_id[record._id] !== undefined) {
            if (record._modified) console.error("Modified record", record._id, "is already present in registry")
            else console.error("Record", record._id, "is already present in registry")
        }
        append_arrdict(records_by_common_id, record.id, record)
        records_by_unique_id[record._id] = record

        // Assign to records-by-file and records-by-type groups
        append_arrdict(records_by_file, source, record)
        append_arrdict(records_by_type, record.type, record)

        // Dependencies
        find_deps_of(record).forEach((dep) => {
            append_arrdict(dep_tree2, dep, record.id)
        })
    })
}

export function mark_file_modified(file: string) {
    if (!modified_files.includes(file)) modified_files.push(file)
}

function find_deps_of(record: Record): string[] {
    const typed_fields = (typed_records[record.type] || {})

    return Object.entries(typed_fields)
        .flatMap(([key, val]) => {
            if (record[key] == undefined) return []
            return get_deps(val, record[key])
        })
}

export function get_deps(descr: field_description | null | undefined, value: any): string[] {
    if (descr == null) return []
    if (value == null) return []

    switch (descr.t) {

        case "ref":
            if (typeof value != "string") return []
            return [value]

        case "array":
            if (!Array.isArray(value)) return []
            return (value as any[]).flatMap((v) => get_deps(descr.arr!!, v))

        case "dict":
            if (value.constructor != Object) return []
            return Object.entries(value).flatMap(([key, val]) => get_deps(descr.dct?.[key], val))

        case "union":
            return descr.uni?.flatMap((t) => get_deps(t, value))!!

    }

    return []
}

export function get_known_files(): string[] {
    return known_files
}

export function get_known_types(): string[] {
    return known_types
}

export function get_changed_files(): [string, Record[]] [] {
    return Object.entries(records_by_file).filter(([file, _]) => modified_files.includes(file))
}

export function get_all_types(): { [key: string]: Record[] } {
    return records_by_type
}

export function reset_records() {

    mod_basedir = {}

    records_by_common_id = {}
    records_by_unique_id = {}
    records_by_file = {}
    records_by_type = {}

    known_types = []
    known_types = []
    modified_files = []

    dep_tree2 = {}
}

export async function get_records(rectype: String): Promise<Record[]> {
    return records_by_type[rectype];
}

export function get_user_deps(ids: OwnRecordId[]): UserDeps {
    let deps: UserDeps = {
        records: {},
        dep_tree: {}
    }

    let processed_ids: string[] = []
    let unprocessed_ids: string[] = ids.map((v) => raw_id(v))

    while (unprocessed_ids.length > 0) {
        let next_unprocessed = []

        unprocessed_ids.forEach((id) => {
            processed_ids.push(id);
            deps.records[id] = records_by_common_id[id];

            (dep_tree2[id] || []).forEach((dep_id) => {
                append_arrdict(deps.dep_tree, id, dep_id);

                if (!processed_ids.includes(dep_id)) {
                    next_unprocessed.push(dep_id);
                }
            })
        })

        unprocessed_ids = next_unprocessed;
    }

    return deps
}

export async function delete_records(records_ids: OwnRecordId[]) {
    records_ids.forEach((record_id) => {
        const record = records_by_unique_id[record_id]
        delete record_id[record]

        drop_arrdict(records_by_file, record._file, record)
        drop_arrdict(records_by_type, record.type, record)

        Object.entries(dep_tree2)
            .forEach(([key, val]) => {
                drop_arrdict(dep_tree2, key, record.id)
            })

        mark_file_modified(record._file!!)
    })
}

export async function write_deps() {
    console.log("Writing deps json")
    let handle = await open("gen/deps.json", 'w')
    await handle.write(JSON.stringify(dep_tree2))
    await handle.close()

    console.log("Writing deps csv")
    handle = await open("gen/deps.csv", 'w')
    await handle.write("source;target")
    for (let [src_record_id, tgt_record_list] of Object.entries(dep_tree2)) {
        for (let tgt_record of tgt_record_list) {
            await handle.write("\n" + src_record_id + ";" + tgt_record)
        }
    }
    await handle.write("\n")
    await handle.close()
}

