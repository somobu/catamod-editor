import {get_all_types, known_ids} from "./model";
import {mkdir, open, readFile} from "fs/promises";
import {on_records_loaded} from "./conversion";
import {internalRecordFields} from "../common/consts";

export type field_type = "todo"  // means 'too lazy to write properly'
    | "id"      // may be string or string-array
    | "bool"    // boolean
    | "int"     // integer
    | "chr"     // single string value 1 char long
    | "str"     // single string value
    | "ref"     // value of this field is a reference to a table `tbl`, field 'id'
    | "enum"    // value of this field is a single item from `enm`
    | "array"   // value of this field is an array, each element is of type `arr`
    | "dict"    // value of this field is a dict which elements described in `dct`
    | "union"   // value of this field can be any of type described in `uni`
    | "strlk"   // string-like type, don't use it

export type field_description = {
    t: field_type,

    tbl?: string,

    enm?: string[]
    arr?: field_description
    dct?: { [key: string]: field_description }
    uni?: field_description[]

    str_vals?: string[]
}

// Primitive types
const todo_t: field_description = {t: "todo"}
const id_t: field_description = {t: "id"}
const bool_t: field_description = {t: "bool"}
const int_t: field_description = {t: "int"}
const chr_t: field_description = {t: "chr"}
const str_t: field_description = {t: "str"}


export let typed_records: { [key: string]: { [key: string]: field_description } } = {}

export async function read_schema_json() {
    try {
        typed_records = JSON.parse(await readFile("gen/schema.json", {encoding: 'utf8'}))
    } catch (e) {
        console.log(e)
    }
}

export async function gen_schema_json(refs_only: boolean = false) {
    typed_records = gen_schema()

    if (refs_only) {
        let clean_records = {}

        Object.entries(typed_records).forEach(([typename, type_dict]) => {

            let type_def = {}
            Object.entries(type_dict).forEach(([field_name, field_type]) => {
                let val = leave_refs_only(field_type)
                if (val != null) type_def[field_name] = val
            })

            clean_records[typename] = type_def
        })

        typed_records = clean_records
    }

    try {
        await mkdir("gen/")
    } catch (_) {
    }

    let handle = await open("gen/schema.json", 'w')
    await handle.write(JSON.stringify(typed_records))
    await handle.close()
}

function gen_schema() {
    let entries = Object.entries(get_all_types())
    entries.sort((a, b) => a[0].localeCompare(b[0]))

    let typed_records: { [key: string]: { [key: string]: field_description } } = {}
    let max_id_len = 0

    entries.forEach(([key, val]) => {
        const rec = {}

        val.forEach(row => {
            if (row.id?.length > max_id_len) {
                max_id_len = row.id.length
            }

            Object.entries(row).forEach(([rec_key, rec_val]) => {
                if (["id", "type", ...internalRecordFields].includes(rec_key)) return;
                if (rec_key.startsWith("//")) return;

                let prev = rec[rec_key]
                let now = deduct(rec_val)
                rec[rec_key] = merge(prev, now)
            })
        })


        // rec["id"] = id_t
        // rec["type"] = type_t
        typed_records[key] = rec
    })

    // console.log("Max id len: " + max_id_len)

    Object.entries(typed_records).forEach(([type_name, type_fields]) => {
        Object.entries(type_fields).forEach(([field_name, field_type]) => {
            guess_strlk(field_name, field_type)
        })
    })

    return typed_records
}

function deduct(value: any): field_description | null {
    if (value == null) return null

    switch (typeof value) {
        case "boolean":
            return bool_t
        case "number":
        case "bigint":
            return int_t
        case "string":
            if (value.length == 1) return chr_t
            else if (value.length > 80) return str_t
            else if (value.includes(" ")) return str_t
            return {t: "strlk", str_vals: [value]}

        case "symbol":
            console.error("deduct: Got symbol value!!")
            return null
        case "function":
            console.error("deduct: Got fcn value!!")
            return null
        case "undefined":
            console.error("deduct: Got undefined value!!")
            return null
    }

    if (Array.isArray(value)) {
        if ((value as any[]).length == 0) {
            return todo_t
        }

        let non_null_sample = (value as any[]).find(v => v != null)
        return {
            t: "array",
            arr: deduct(non_null_sample!!)!!
        }
    } else {
        let dict: field_description = {
            t: "dict",
            dct: {}
        }
        Object.entries(value).forEach(([key, val]) => {
            if (key.startsWith("//")) return;
            dict.dct!![key] = deduct(val)!!
        })
        return dict
    }
}

function can_merge(a: field_description, b: field_description): boolean {
    if (a == null || b == null) {
        let e = 0;
    }

    if (a.t == "todo" || b.t == "todo") return true
    if (a.t == "str" && b.t == "strlk") return true
    if (a.t == "strlk" && b.t == "str") return true

    if (a.t != b.t) return false;


    switch (a.t) {
        case "id":
        case "bool":
        case "int":
        case "chr":
        case "str":
        case "strlk":
        case "enum":
        case "union":
            return true;

        case "ref":
            return a.tbl == b.tbl;

        case "array":
            return can_merge(a.arr!!, b.arr!!)

        case "dict":
            const hasConflict = Object.entries(a.dct!!).some(([k, v]) => {
                if (a.dct!![k] == null) return false // No value, so no conflict
                else return !can_merge(v, a.dct!![k])
            })
            return !hasConflict
    }

    return false
}

function merge(a: field_description | null | undefined, b: field_description | null | undefined): field_description | null {
    if (a == null && b == null) return null
    if (a == null) return b!!
    if (b == null) return a
    if (a.t == "todo") return b
    if (b.t == "todo") return a

    // Here a and b both are not null, lets compare
    if (a.t == b.t) {
        switch (a.t) {
            case "id":
            case "bool":
            case "int":
            case "chr":
            case "str":
                return a;
            case "strlk":
                return {
                    t: "strlk",
                    str_vals: [...new Set([...a.str_vals, ...b.str_vals])]
                }

            case "array":
                return {
                    t: "array",
                    arr: merge(a.arr, b.arr)!!
                }

            case "dict":
                Object.entries(a.dct).forEach(([key, val]) => {
                    a.dct!![key] = merge(a.dct!![key], b.dct!![key])!!
                })
                Object.entries(b.dct).forEach(([key, val]) => {
                    a.dct!![key] = merge(a.dct!![key], b.dct!![key])!!
                })
                return a;
            case "union":
                // Append b's types to union a
                b.uni?.forEach(t => a = union_merge(a, t))
                return a;

            case "ref":
            case "enum":
                console.error("Somehow got ref or enum!");
                return a;

        }
    } else {
        if (a.t == "str" && b.t == "strlk") return a;
        if (a.t == "strlk" && b.t == "str") return b;

        if (a.t == "union") {
            return union_merge(a, b)
        } else if (b.t == "union") {
            return union_merge(b, a)
        } else {
            let uni: field_description = {
                t: "union",
                uni: [a]
            }
            uni = union_merge(uni, b)
            return uni
        }
    }

    return b;
}

function union_merge(union: field_description, value: field_description): field_description {

    for (let i = 0; i < union.uni?.length; i++) {
        let target = union.uni!![i]
        if (can_merge(target, value)) {
            merge(target, value)
            return union
        }
    }

    union.uni = [...union.uni!!, value]
    return union
}

function guess_strlk(field_name: string, field: field_description) {
    if (field == null) {
        console.log("NULL field!!!")
        return
    }

    switch (field.t) {
        case "todo":
        case "id":
        case "bool":
        case "int":
        case "chr":
        case "str":
        case "ref":
        case "enum":
            field.enm?.sort((a, b) => a.localeCompare(b))
            break;
        case "strlk":
            let is_ref = field.str_vals!!.every((v) => known_ids.includes(v))

            if (is_ref) {
                field.t = "ref"
            } else {
                field.t = "enum"
                field.enm = field.str_vals
                field.enm?.sort((a, b) => a.localeCompare(b))
            }
            delete field["str_vals"]

            break;
        case "array":
            guess_strlk(field_name, field.arr!!)
            break;
        case "dict":
            Object.entries(field.dct!!).forEach(([k, v]) => guess_strlk(field_name, v))
            break;
        case "union":
            Object.entries(field.uni!!).forEach(([k, v]) => guess_strlk(field_name, v))
            break;

    }
}

export async function regen_schema() {
    console.log("Generating schema json")
    await gen_schema_json(true)
    console.log("Schema json generated successfully")
    
    await on_records_loaded()
}

export function leave_refs_only(d: field_description): field_description | null {

    switch (d.t) {
        case "ref":
            return d

        case "array":
            let clean_arr = leave_refs_only(d.arr!!)
            if (clean_arr == null) return null
            else return {t: "array", arr: clean_arr}

        case "dict":
            let clean_dict = {}

            for (const [k, v] of Object.entries(d.dct)) {
                const ccc = leave_refs_only(v)
                if (ccc != null) clean_dict[k] = ccc
            }

            if (Object.keys(clean_dict).length === 0) return null
            else return {t: "dict", dct: clean_dict}

        case "union":
            let clean_uni = []
            d.uni!!.forEach(v => {
                const clean = leave_refs_only(v)
                if (clean != null) clean_uni.push(clean)
            })

            if (clean_uni.length == 0) return null
            else return {t: "union", uni: clean_uni}

    }

    return null
}
