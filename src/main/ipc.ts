import IpcMainEvent = Electron.IpcMainEvent;
import {ipcMain} from "electron";
import {get_modlist, load_mods} from "./reader";
import {delete_records, get_records, get_user_deps, reset_records, write_deps} from "./model";
import {write_changed} from "./writer";
import {regen_schema} from "./schema";
import {OwnRecordId} from "../common/types";

let api = {
    'get-modlist': get_modlist,
    'load-mods': load_mods,

    'reset-records': async (_: any) => reset_records(),
    'get-records': get_records,
    'get-deps': async (ids: OwnRecordId[]) => get_user_deps(ids),

    'delete-records': delete_records,

    'save-records': write_changed,
    'gen-schema': regen_schema,
    'write-deps': write_deps,
}


export const regApi = () => {
    for (const [key, value] of Object.entries(api)) {
        ipcMain.on(key, async (event: IpcMainEvent, args: any[]) => {
            event.reply(key, await value(args))
        });
    }
}
