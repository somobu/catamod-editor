import {uuidv4} from "./util";
import {dep_tree2, mark_file_modified} from "./model";
import {Record} from "../common/types";
import {read_schema_json} from "./schema";

const DEBUG_GENERATED_IDS = true;

export async function on_before_records_load() {
    console.log("Schema json reading")
    await read_schema_json()
    console.log("Schema json reading: done")
}

export function on_record_read(record: Record): Record[] {
    validate_id(record)
    ensure_no_ident(record)

    let records = unwrap_array_ids(record)

    // ensure_no_array_ids(record)

    return records
}

export async function on_records_loaded() {
    console.log("Post-processing records")

    console.log("Filtering dep tree")
    let filtered_dep_tree: { [key: string]: string[] } = {}
    Object.entries(dep_tree2).forEach(([src_record_id, tgt_record_list]) => {
        let list = []
        tgt_record_list.forEach((tgt_record) => {
            if (!list.includes(tgt_record)) list.push(tgt_record)
        })
        filtered_dep_tree[src_record_id] = list
    })
    
}

function validate_id(record: Record) {
    const substitute_name = ["monstergroup", "EXTERNAL_OPTION", "MONSTER_FACTION"]

    if (record.id == undefined) {
        if (record["abstract"] !== undefined) {
            if (DEBUG_GENERATED_IDS) console.error("Substitute abstract for " + record.type + ":" + String(record["abstract"]))
            record.id = String(record["abstract"])
            record._modified = true
            mark_file_modified(record._file!!)

        } else if (record["ident"] !== undefined) {
            if (DEBUG_GENERATED_IDS) console.error("Substitute ident for " + record.type + ":" + String(record["abstract"]))
            record.id = String(record["ident"])
            delete record["ident"]
            record._modified = true
            mark_file_modified(record._file!!)

        } else if (substitute_name.includes(record.type) && record["name"] !== undefined) {
            record.id = String(record["name"])
            record._modified = true
            mark_file_modified(record._file!!)

        } else {
            if (DEBUG_GENERATED_IDS) console.error("Assign UUID for " + record.type)
            record.id = String(uuidv4())
            record._modified = true
            mark_file_modified(record._file!!)
        }

    }
}

function ensure_no_ident(record: Record) {
    if (record["ident"] !== undefined) {
        console.error("ALARM, got ident field on " + record.type + " " + record.id);
    }
}

function unwrap_array_ids(record: Record): Record[] {
    let id = record.id

    // noinspection SuspiciousTypeOfGuard
    if (!(typeof id === 'string' || id instanceof String)) {
        if (!Array.isArray(id)) {
            console.error("ALARM, got non-string id of " + record.type + " " + record.id);
        } else {
            console.error("Unrolling array ids of ", record.type, " ", record.id)

            const original = JSON.stringify(record)
            const first_id = (id as string[])[0]

            return (id as string[]).map((id) => {
                let modified = JSON.parse(original) as Record
                modified["unrolled_id_group"] = first_id
                modified.id = id
                modified._modified = true
                mark_file_modified(record._file!!)
                return modified
            })
        }
    } else {
        return [record]
    }
}
