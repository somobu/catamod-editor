import {get_changed_files} from "./model";
import {open} from "fs/promises";
import {internalRecordFields} from "../common/consts";


export async function write_changed() {
    console.log("Here we'll write dat data")

    const files = get_changed_files()
    for (let i = 0; i < files.length; i++) {
        const filename = files[i][0]

        const records = files[i][1].map((record) => {
            const deepCopied = JSON.parse(JSON.stringify(record));
            internalRecordFields.forEach((field) => delete deepCopied[field])
            return deepCopied
        })

        console.log("Writing to " + filename)

        let handle = await open(filename, 'w')
        await handle.write(JSON.stringify(records, null, 2))
        await handle.close()
    }

}
