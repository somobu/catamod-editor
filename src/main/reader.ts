import {readdir, readFile} from "fs/promises";
import {
    accumulate,
    get_known_files,
    get_known_types,
    mod_basedir,
    reset_records
} from "./model";
import {Dirent} from "fs";
import {on_before_records_load, on_records_loaded} from "./conversion";
import {Record, Record_ModInfo} from "../common/types";

const DEBUG_LOADING = false;

export type ModLoadingResult = {
    known_files: string[],
    known_types: string[]
}

async function process_dir(mod_id: string, dir: string) {
    if (DEBUG_LOADING) console.log("Processing dir " + dir)

    const files = await readdir(dir, {withFileTypes: true});
    for (const file: Dirent of files) {

        if (file.isFile() && file.name.endsWith(".json")) {
            const filename = dir + "/" + file.name
            if (DEBUG_LOADING) console.log("  Read file " + filename)

            const startTime = performance.now()

            const entries = await read_json(filename);
            entries.forEach((entry) => accumulate(mod_id, filename, entry));

            if (DEBUG_LOADING) console.log("  File processed in " + (performance.now() - startTime) + " ms")
        }
    }

    if (DEBUG_LOADING) console.log("Dir processed!")

    for (const local_dir: Dirent of files) {
        if (local_dir.isDirectory()) {
            await process_dir(mod_id, dir + "/" + local_dir.name)
        }
    }
}

export async function read_json(path: string): Promise<Record[]> {
    const data = await readFile(path, {encoding: 'utf8'});
    return JSON.parse(data);
}


export function get_records(data: Record[], record_type: string): Record[] {
    return data.filter((value) => value.type === record_type)
}

export async function load_mods(data: Record_ModInfo[]): Promise<ModLoadingResult> {

    reset_records();

    await on_before_records_load()

    for (const mod of data) {
        if (mod.path != null) mod_basedir[mod] = mod._modinfo_dir + "/" + (mod.path || "")
        else mod_basedir[mod] = mod.path!!

        await process_dir(mod.id, mod._modinfo_dir + "/");
        if (mod.path != null) await process_dir(mod.id, mod._modinfo_dir + "/" + (mod.path || ""));
    }

    await on_records_loaded()

    return {
        known_files: get_known_files(),
        known_types: get_known_types().sort()
    }
}


export async function get_modlist(args: any[]): Promise<Record_ModInfo[]> {

    let mods: Record_ModInfo[] = [];

    try {
        mods.push(...await get_mods_from_dir("../data/mods"));
        mods.push(...await get_mods_from_dir("../user/mods"));
    } catch (err) {
        console.error(err);
    }

    mods.sort((a, b) => a.id.localeCompare(b.id))

    let sortedMods: Record_ModInfo[] = [];
    let sortedIds: String[] = [];

    while (mods.length > 0) {
        for (let i = 0; i < mods.length; i++) {
            const mod: Record_ModInfo = mods[i];

            const has_other_deps = mod.dependencies?.some((value) => !sortedIds.includes(value));
            if (!has_other_deps) {
                sortedMods.push(mod);
                sortedIds.push(mod.id);
                mods = mods.filter((_, index) => index !== i);
                break;
            }

            const missing_ids = mod.dependencies?.flatMap((value) => {
                const missing_in_sorted = !sortedIds.some((id) => id === value)
                const missing_in_unsorted = !mods.some((mod) => mod.id === value)

                if (missing_in_sorted && missing_in_unsorted) return value
                else return []
            })

            if (missing_ids?.length > 0) {
                console.error("Mod " + mod.id + " depends on missing " + missing_ids)
                mods = mods.filter((_, index) => index !== i);
                break;
            }
        }
    }

    return sortedMods;
}

async function get_mods_from_dir(path: String): Promise<Record_ModInfo[]> {

    let mods: Record_ModInfo[] = [];

    try {
        const files = await readdir(path, {withFileTypes: true});
        for (const dir: Dirent of files) {
            if (dir.isDirectory()) {
                const dirpath = path + "/" + dir.name;
                try {
                    const entries = await read_json(dirpath + "/modinfo.json");
                    const modinfo = get_records(entries, "MOD_INFO")[0] as Record_ModInfo;
                    modinfo.dependencies = modinfo.dependencies || [];
                    modinfo._modinfo_dir = dirpath;
                    mods.push(modinfo);
                } catch (e) {
                    console.error("no modinfo at " + dirpath + ", skipping");
                }

            }
        }
    } catch (err) {
        console.error(err);
    }

    return mods;
}
